# athina's documentation

## Introduction

athina is a data-analysis package based on Igor Pro (verion 9 or newer) for the analysis of LEEM, LEED, PEEM, XPS, ARPES, XAS, STM data to name a few. The motivation and focus of athina's development was the users of the MAXPEEM beamline at MAXIV synchrotron in Lund, Sweden, but it can be used by anyone who wants to analyse (photo-)electron microscopy data or any image-based datasets.

See [](files/installation.md) on how to install _athina_ on your Windows or MacOS computer.


## Internal links

* [athina - source code at MAXIV gitlab](https://gitlab.maxiv.lu.se/evagko/athina)

## External links

* [athina - GitLab](https://gitlab.com/evangelosgolias/athina)
* [Igor Pro 9 manual (pdf)](http://www.wavemetrics.net/doc/IgorMan.pdf)
* [Wavemetrics](https://www.wavemetrics.com)

```{toctree}
---
caption: Table of Contents
name: toc
---

Installation <files/installation.md>
The basics <files/thebasics.md>
Operations <files/operations.md>
Useful features <files/other_operations.md>
How-to guides <files/howto.md>
Best practices <files/best_practices.md>
```

Build time: {sub-ref}`today`