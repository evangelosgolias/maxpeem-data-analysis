# Introduction

**athina** is a data analysis package for spectromicroscopy/microspectroscopy experiments based on Igor Pro 9 or later. It can analyse datasets from PEEM, LEEM, XPEEM, XMCD/XMLD-PEEM, μ-XPS, PED, ARPES and more image-based data.

It has built-in support for reading Elmitec's proprietary .dat files, EMMENU tif/tif stacks, NeXus files and more.

## Documentation

*athina's* documentation: https://evangelosgolias.gitlab.io/athina/

to access the documentation from the intranet of MAXIV synchrotron facility: https://evagko.gitlab-pages.maxiv.lu.se/athina/
